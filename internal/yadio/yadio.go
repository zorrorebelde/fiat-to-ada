package yadio

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/shopspring/decimal"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/ua"
)

const (
	defaultBaseURL = "https://api.yadio.io"
	decimalDigits  = 8
)

type exRateResponse struct {
	Request exRateRequest `json:"request"`
	Result  float64       `json:"result"`
	Rate    float64       `json:"rate"`
	// Timestamp time.Time     `json:"timestamp"`
}

type exRateRequest struct {
	Amount float64 `json:"amount"`
	From   string  `json:"from"`
	To     string  `json:"to"`
}

type YadioIOClient struct {
	BaseURL   *url.URL
	UserAgent string

	httpClient *http.Client
}

// ConvertToBTC tells how much fiat is BTC worth
//
// Data fetch from Yadio.io
func (c *YadioIOClient) ConvertToBTC(fiat string, amount decimal.Decimal) (*decimal.Decimal, *decimal.Decimal, error) {
	rel := &url.URL{
		Path: fmt.Sprintf("convert/%s/%s/BTC", amount.StringFixed(decimalDigits), fiat),
	}
	u := c.BaseURL.ResolveReference(rel)
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()
	var response exRateResponse

	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return nil, nil, err
	}

	dAmount := decimal.NewFromFloat(response.Result)
	dRate := decimal.NewFromFloat(response.Rate)

	return &dAmount, &dRate, nil
}

func (c *YadioIOClient) listCurrencies() (map[string]string, error) {
	rel := &url.URL{
		Path: "currencies",
	}
	u := c.BaseURL.ResolveReference(rel)
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var fiat map[string]string

	err = json.NewDecoder(resp.Body).Decode(&fiat)

	if err != nil {
		return nil, err
	}

	return fiat, nil
}

func (c *YadioIOClient) ListCurrencies() ([]string, []string, error) {
	fiats, err := c.listCurrencies()
	if err != nil {
		return nil, nil, err
	}

	var list []string
	var codes []string
	for code, desc := range fiats {
		list = append(list, fmt.Sprintf("%s: %s ", code, desc))
		codes = append(codes, code)
	}

	return list, codes, nil
}

func New(options ...ConfigOption) (*YadioIOClient, error) {
	conf := &config{}
	for _, o := range options {
		o.apply(conf)
	}

	u := defaultBaseURL
	if conf.Url != "" {
		u = conf.Url
	}

	userAgent := ua.UserAgent
	if conf.UserAgent != "" {
		userAgent = conf.UserAgent
	}

	yadioURL, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	yio := &YadioIOClient{
		BaseURL:    yadioURL,
		UserAgent:  userAgent,
		httpClient: &http.Client{},
	}

	return yio, nil
}
