package yadio

type ConfigOption interface {
	apply(opts *config)
}

type yadioBaseURL string

func (y yadioBaseURL) apply(c *config) {
	c.Url = string(y)
}

func WithAPIURL(baseURL string) ConfigOption {
	return yadioBaseURL(baseURL)
}

type yadioUserAgent string

func (y yadioUserAgent) apply(c *config) {
	c.UserAgent = string(y)
}

func WithUserAgent(ua string) ConfigOption {
	return yadioUserAgent(ua)
}
