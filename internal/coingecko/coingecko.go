package coingecko

import (
	"encoding/json"
	"net/http"
	"net/url"

	"github.com/shopspring/decimal"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/ua"
)

const (
	defaultBaseURL = "https://api.coingecko.com"
)

type simpleExchangeRate struct {
	BTC float64 `json:"btc"`
}

type simpleResponse struct {
	Cardano simpleExchangeRate `json:"cardano"`
}

type CoinGeckoClient struct {
	BaseURL   *url.URL
	UserAgent string

	httpClient *http.Client
}

// GetSimplePriceADAToBTC returns how much Bitcoin is 1 ADA worth
func (c *CoinGeckoClient) GetSimplePriceADAToBTC() (*decimal.Decimal, error) {
	rel := &url.URL{
		Path:     "api/v3/simple/price",
		RawQuery: "ids=cardano&vs_currencies=btc&precision=full",
	}
	u := c.BaseURL.ResolveReference(rel)
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var rate simpleResponse

	err = json.NewDecoder(resp.Body).Decode(&rate)
	if err != nil {
		return nil, err
	}

	amount := decimal.NewFromFloat(rate.Cardano.BTC)

	return &amount, nil
}

func New(options ...ConfigOption) (*CoinGeckoClient, error) {
	conf := &config{}
	for _, o := range options {
		o.apply(conf)
	}

	u := defaultBaseURL
	if conf.Url != "" {
		u = conf.Url
	}

	userAgent := ua.UserAgent
	if conf.UserAgent != "" {
		userAgent = conf.UserAgent
	}

	cgURL, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	cg := &CoinGeckoClient{
		BaseURL:    cgURL,
		UserAgent:  userAgent,
		httpClient: &http.Client{},
	}

	return cg, nil
}
