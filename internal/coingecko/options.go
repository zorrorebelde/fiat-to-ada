package coingecko

type ConfigOption interface {
	apply(opts *config)
}

type coingeckoAPIUrl string

func (g coingeckoAPIUrl) apply(c *config) {
	c.Url = string(g)
}

func WithAPIURL(baseURL string) ConfigOption {
	return coingeckoAPIUrl(baseURL)
}

type coingeckoUserAgent string

func (g coingeckoUserAgent) apply(c *config) {
	c.UserAgent = string(g)
}

func WithUserAgent(ua string) ConfigOption {
	return coingeckoUserAgent(ua)
}
