package exrate

import (
	"github.com/shopspring/decimal"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/coingecko"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/yadio"
)

const (
	adaprecision = 6
)

func GetExchangeRate(fiat string, amount decimal.Decimal) (string, string, error) {
	cgc, err := coingecko.New()
	if err != nil {
		return "", "", err
	}

	yac, err := yadio.New()
	if err != nil {
		return "", "", err
	}

	base, err := cgc.GetSimplePriceADAToBTC()
	if err != nil {
		return "", "", err
	}

	exAmount, rate, err := yac.ConvertToBTC(fiat, amount)
	if err != nil {
		return "", "", err
	}

	result := exAmount.DivRound(*base, adaprecision)
	adaRate := rate.DivRound(*base, adaprecision)

	return result.String(), adaRate.String(), nil
}

func IsCodeSupported(fiat string, list []string) bool {
	for _, code := range list {
		if fiat == code {
			return true
		}
	}

	return false
}
