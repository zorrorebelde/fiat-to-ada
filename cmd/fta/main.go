package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/fatih/color"
	"github.com/shopspring/decimal"
	flag "github.com/spf13/pflag"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/exrate"
	"gitlab.com/zorrorebelde/fiat-to-ada/internal/yadio"
)

var (
	amount   float64
	code     string
	listFiat bool
	help     bool
)

func init() {
	flag.BoolVar(&listFiat, "supported-currencies", false, "list the current supported currencies from yadio.io")
	flag.BoolVar(&help, "help", false, "show help")
	flag.Usage = myUsage
}

func description() {
	fmt.Printf("get the exchange rate of your local currency to ADA using Bitcoin for the rate.\ndata provided for free by %s and %s.\n\n", color.GreenString("CoinGecko"), color.YellowString("Yadio.io"))
}

func myUsage() {
	fmt.Printf("Usage: %s <amount> <currency>\nExample: %s 10 usd\n\n", os.Args[0], os.Args[0])
	flag.PrintDefaults()
}

func main() {
	errorf := color.New(color.FgRed).PrintfFunc()
	blue := color.New(color.FgBlue).PrintfFunc()
	blueBold := color.New(color.FgBlue, color.Bold).PrintfFunc()

	flag.Parse()

	commandPassed := false

	// check if the minimal amount of positional arguments were pass
	args := flag.Args()
	if len(args) == 2 {
		result, err := strconv.ParseFloat(args[0], 64)
		if err != nil {
			errorf("invalid argument %s for amount, error: %v", args[0], err)
			os.Exit(2)
		}
		commandPassed = true

		amount = result
		code = args[1]
	}

	flag.Visit(func(f *flag.Flag) {
		if f.Name == "supported-currencies" {
			commandPassed = true
		}
	})

	if help || !commandPassed {
		if !commandPassed {
			errorf("ERR: required options or positional arguments not passed!\n\n")
		}
		description()
		flag.Usage()

		if !commandPassed {
			os.Exit(1)
		}

		os.Exit(0)
	}

	yac, err := yadio.New()
	if err != nil {
		errorf("unable to create yadio.io client, error: %v", err)
		os.Exit(1)
	}

	fiats, codes, err := yac.ListCurrencies()
	if err != nil {
		errorf("unable to fetch list of supported currencies from yadio.io, error: %v", err)
	}

	if listFiat {
		sort.Strings(fiats)

		fmt.Printf("list of supported currencies:\n\n  %s\n", strings.Join(fiats, "\n  "))
		os.Exit(0)
	}

	if code == "" {
		errorf("invalid code: empty string")
		os.Exit(1)
	} else {
		code = strings.ToUpper(code)
		supported := exrate.IsCodeSupported(code, codes)

		if !supported {
			errorf("Currency not supported: " + code)
			os.Exit(2)
		}
	}

	if amount <= 0 {
		errorf("%0.2f %s is an invalid amount", amount, code)
		os.Exit(2)
	}

	result, rate, err := exrate.GetExchangeRate(code, decimal.NewFromFloat(amount))
	if err != nil {
		errorf("unable to calculate the exchange rate for %0.2f %s, error: ", amount, code, err)
		os.Exit(1)
	}

	blue("%0.2f %s equals to %s ADA", amount, code, result)
	blueBold("\nrate: 1 %s = %s ADA\n", code, rate)
}
