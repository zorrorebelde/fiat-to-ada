module gitlab.com/zorrorebelde/fiat-to-ada

go 1.19

require (
	github.com/fatih/color v1.14.1
	github.com/shopspring/decimal v1.3.1
	github.com/spf13/pflag v1.0.5
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
