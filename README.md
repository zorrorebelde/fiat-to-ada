# Fiat to ADA
calculate the exchange rate between your local currency and Cardano ADA using Bitcoin for the rate.

Data provided for free by [CoinGecko](https://coingecko.com) and [Yadio.io](https://yadio.io).

## Why?

I imagine a light wallet for Cardano that people around the world can use for their day to day financial needs, a wallet that is beautiful, fast and user-friendly, but getting the exchange rate of Cardano ADA to your local currency may be troublesome since not that many services offers your favorite fiat.

This is an experiment to figure out if using Yadio.io and CoinGecko can be combined to provide an "ADA to local fiat" exchange rate.

## how to install

``` shell
go install gitlab.com/zorrorebelde/fiat-to-ada/cmd/fta@latest
```

## Usage

Let's say you want to know how much 50 USD are in Cardano ADA.

``` shell
fta 50 usd
```

Outputs:

``` text
50.00 USD equals to 127.342124 ADA
rate: 1 USD = 2.546846 ADA
```

## Supported currencies.

As of Feb 5th, 2023, these are the supported list of currencies:

- AED: United Arab Emirates Dirham 
- ANG: Netherlands Antillean Guilder 
- AOA: Angolan Kwanza 
- ARS: Argentine Peso 
- AUD: Australian Dollar 
- AZN: Azerbaijani Manat 
- BDT: Bangladeshi Taka 
- BHD: Bahraini Dinar 
- BMD: Bermudan Dollar 
- BOB: Bolivian Boliviano 
- BRL: Brazilian Real 
- BTC: Bitcoin 
- BWP: Botswanan Pula 
- BYN: Belarusian Ruble 
- CAD: Canadian Dollar 
- CDF: Congolese Franc 
- CHF: Swiss Franc 
- CLP: Chilean Peso 
- CNY: Chinese Yuan 
- COP: Colombian Peso 
- CRC: Costa Rican Colón 
- CUP: Cuban Peso 
- CZK: Czech Republic Koruna 
- DKK: Danish Krone 
- DOP: Dominican Peso 
- DZD: Algerian Dinar 
- EGP: Egyptian Pound 
- ETB: Ethiopian Birr 
- EUR: Euro 
- GBP: British Pound Sterling 
- GEL: Georgian Lari 
- GHS: Ghanaian Cedi 
- GTQ: Guatemalan Quetzal 
- HKD: Hong Kong Dollar 
- HNL: Honduran Lempira 
- HUF: Hungarian Forint 
- IDR: Indonesian Rupiah 
- ILS: Israeli New Sheqel 
- INR: Indian Rupee 
- IRR: Iranian Rial 
- IRT: Iranian Tomans 
- JMD: Jamaican Dollar 
- JOD: Jordanian Dinar 
- JPY: Japanese Yen 
- KES: Kenyan Shilling 
- KGS: Kyrgystani Som 
- KRW: South Korean Won 
- KZT: Kazakhstani Tenge 
- LBP: Lebanese Pound 
- LKR: Sri Lankan Rupee 
- MAD: Moroccan Dirham 
- MLC: Cuban MLC 
- MXN: Mexican Peso 
- MYR: Malaysian Ringgit 
- NAD: Namibian Dollar 
- NGN: Nigerian Naira 
- NIO: Nicaraguan Córdoba 
- NOK: Norwegian Krone 
- NPR: Nepalese Rupee 
- NZD: New Zealand Dollar 
- PAB: Panamanian Balboa 
- PEN: Peruvian Nuevo Sol 
- PHP: Philippine Peso 
- PKR: Pakistani Rupee 
- PLN: Polish Zloty 
- PYG: Paraguayan Guarani 
- QAR: Qatari Rial 
- RON: Romanian Leu 
- RSD: Serbian Dinar 
- RUB: Russian Ruble 
- SAR: Saudi Riyal 
- SEK: Swedish Krona 
- SGD: Singapore Dollar 
- THB: Thai Baht 
- TND: Tunisian Dinar 
- TRY: Turkish Lira 
- TTD: Trinidad and Tobago Dollar 
- TWD: New Taiwan Dollar 
- TZS: Tanzanian Shilling 
- UAH: Ukrainian Hryvnia 
- UGX: Ugandan Shilling 
- USD: United States Dollar 
- UYU: Uruguayan Peso 
- UZS: Uzbekistan Som 
- VES: Venezuelan Bolívar Soberano 
- VND: Vietnamese Dong 
- XAF: CFA Franc BEAC 
- XAG: Silver Ounce 
- XAU: Gold Ounce 
- XOF: CFA Franc BCEAO 
- XPT: Platinum Ounce 
- ZAR: South African Rand

**NOTE**: We depend upon Yadio.io for this support since we use their exchange rate of Bitcoin.
